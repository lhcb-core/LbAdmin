LHCb Admin tools 
====================================
|pipeline status| |coverage report|


Administration and releae tools for the LHCb environment.

.. |pipeline status| image:: https://gitlab.cern.ch/lhcb-core/LbAdmin/badges/master/pipeline.svg
                     :target: https://gitlab.cern.ch/lhcb-core/LbAdmin/commits/master
.. |coverage report| image:: https://gitlab.cern.ch/lhcb-core/LbAdmin/badges/master/coverage.svg
                     :target: https://gitlab.cern.ch/lhcb-core/LbAdmin/commits/master
